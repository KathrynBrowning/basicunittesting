package edu.westga.smarttherm.model.tests;

import static org.junit.Assert.*;

import org.junit.Before;

import org.junit.Test;

import edu.westga.smarttherm.model.SmartThermostat;

/**
 * Test cases for the SmartThermostat class
 * @author Kathryn Browning
 *
 */
public class WhenChangingDesiredTemperature {

	private SmartThermostat st;

	/**
	 * Set-Up method for the test cases.
	 * @throws Exception if the SmartThermostat object cannot be made.
	 */
	@Before
	public void setUp() throws Exception {
		this.st = new SmartThermostat();
	}

	/**
	 * Temperature should be set to the min temp if set to 32.
	 */
	@Test
	public void shouldSetAtTempMin() {
		this.st.setDesiredTemperature(32);
		assertEquals(32, this.st.getDesiredTemperature());
	}

	/**
	 * Temperature should be set to the max temp if set to 120.
	 */
	@Test
	public void shouldSetAtTempMax() {
		this.st.setDesiredTemperature(120);
		assertEquals(120, this.st.getDesiredTemperature());
	}

	/**
	 * If the temperature is in between the min and max temps, then the
	 * temperature should be set to that temperature.
	 */
	@Test
	public void shouldSetAtTempWithinMaxAndMin() {
		this.st.setDesiredTemperature(74);
		assertEquals(74, this.st.getDesiredTemperature());
	}

	/**
	 * Temperature should be set at default temperature if no temperature is
	 * given.
	 */
	@Test
	public void shouldBeDefaultTempIfNoTempGiven() {
		assertEquals(65, this.st.getDesiredTemperature());
	}

	/**
	 * If the temperature is lower than the min. temp., then the temperature
	 * should be set to the min. temp.
	 */
	@Test
	public void shouldSetAtMinTempIfParamLessThanMinTemp() {
		this.st.setDesiredTemperature(25);
		assertEquals(32, this.st.getDesiredTemperature());
	}

	/**
	 * If the temperature is higher than the max. temp., then the temperature
	 * should be set to the max. temp.
	 */
	@Test
	public void shouldSetAtTempAtMaxTempIfParamGreaterThanMaxTemp() {
		this.st.setDesiredTemperature(400);
		assertEquals(120, this.st.getDesiredTemperature());
	}

	/**
	 * After multiple temperatures have been entered, the desired temperature
	 * should always be the last one entered.
	 */
	@Test
	public void desiredTempShouldAlwaysBeLatestSetTemp() {
		this.st.setDesiredTemperature(15);
		this.st.setDesiredTemperature(40);
		this.st.setDesiredTemperature(67);
		this.st.setDesiredTemperature(80);
		assertEquals(80, this.st.getDesiredTemperature());
	}

	/**
	 * Temperature should be set to the min temp if the temperatures is set to
	 * 0.
	 */
	@Test
	public void shouldSetAtMinTempIfTempSetIsZero() {
		this.st.setDesiredTemperature(0);
		assertEquals(32, this.st.getDesiredTemperature());
	}
}
