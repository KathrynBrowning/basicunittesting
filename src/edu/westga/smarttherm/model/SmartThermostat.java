package edu.westga.smarttherm.model;

import java.time.Clock;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Represents a smart thermostat controller that minimizes energy consumption and maximizes comfort based on user preferences.
 *
 * @author lewisb
 *
 */
public class SmartThermostat {
	private ArrayList<SetPoint> setPoints;
	private int desiredTemp;
	private Clock clock;

	private static final int DEFAULT_TEMP = 65;
	public static final int TEMP_MIN = 32;
	public static final int TEMP_MAX = 120;


	/**
	 * Creates a new SmartThermostat using the given thermometer.
	 */
	public SmartThermostat() {
		this.setPoints = new ArrayList<SetPoint>();
		this.desiredTemp = DEFAULT_TEMP;
		this.clock = Clock.systemUTC();
	}


	/**
	 * Sets the thermostat to the desired temperature and notifies the SmartThermostat
	 * that this is the desired temperature for this time of day.  Will not set lower than TEMP_MIN or higher
	 * than TEMP_MAX
	 *
	 * @param temp the desired temperature
	 */
	public void setDesiredTemperature(int temp) {
		if (temp < TEMP_MIN) {
			temp = TEMP_MIN;
		}

		if (temp > TEMP_MAX) {
			temp = TEMP_MAX;
		}

		this.desiredTemp = temp;
		SetPoint setPoint = new SetPoint(temp, LocalTime.now(this.clock));
		this.setPoints.add(setPoint);
		Collections.sort(this.setPoints);
	}

	/**
	 * Gets the desired temperature
	 * @return the desired temperature
	 */
	public int getDesiredTemperature() {
		return this.desiredTemp;
	}

	/**
	 * Gets the daily schedule of set points, ordered from earliest to latest beginning at midnight.
	 * @return the daily schedule of set points
	 */
	public SetPoint[] getSchedule() {
		return this.setPoints.toArray(new SetPoint[0]);
	}
}
